/// @file unplaced_root_volume.h
/// @author Johannes de Fine Licht (johannes.definelicht@cern.ch)

#pragma once

#include "VecGeom/base/Global.h"
#include "VecGeom/volumes/UnplacedVolume.h"
#include <TGeoShape.h>

namespace vecgeom {
namespace tgeo2vecgeom {

class UnplacedRootVolume : public VUnplacedVolume {

private:
  UnplacedRootVolume(const UnplacedRootVolume &);            // Not implemented
  UnplacedRootVolume &operator=(const UnplacedRootVolume &); // Not implemented

  TGeoShape const *fRootShape;

public:
  UnplacedRootVolume(TGeoShape const *const rootShape) : fRootShape(rootShape){};

  virtual ~UnplacedRootVolume() = default;

  TGeoShape const *GetRootShape() const { return fRootShape; }

  bool Contains(Vector3D<Precision> const &p) const override { return fRootShape->Contains(&Vector3D<double>(p)[0]); }

  EnumInside Inside(Vector3D<Precision> const &point) const override
  {
    return Contains(point) ? static_cast<EnumInside>(EInside::kInside) : static_cast<EnumInside>(EInside::kOutside);
  }

  Precision DistanceToIn(Vector3D<Precision> const &position, Vector3D<Precision> const &direction,
                         const Precision stepMax) const override
  {
    return GetRootShape()->DistFromOutside(&Vector3D<double>(position)[0], &Vector3D<double>(direction)[0], 3);
  }

  Precision DistanceToOut(Vector3D<Precision> const &position, Vector3D<Precision> const &direction,
                          const Precision stepMax) const override
  {
    return GetRootShape()->DistFromInside(&Vector3D<double>(position)[0], &Vector3D<double>(direction)[0], 3);
  }

  Precision SafetyToOut(Vector3D<Precision> const &position) const override
  {
    return GetRootShape()->Safety(&Vector3D<double>(position)[0], true);
  }

  Precision SafetyToIn(Vector3D<Precision> const &position) const override
  {
    return GetRootShape()->Safety(&Vector3D<double>(position)[0], false);
  }

  Precision Capacity() const override { return GetRootShape()->Capacity(); }

  Precision SurfaceArea() const override { return 0.; /*GetRootShape()->SurfaceArea();*/ }

  int MemorySize() const override { return sizeof(*this); }

  void Print() const override;

  void Print(std::ostream &os) const override;

private:
  virtual VPlacedVolume *SpecializedVolume(LogicalVolume const *const volume,
                                           Transformation3D const *const transformation,
                                           const TranslationCode trans_code, const RotationCode rot_code,
                                           VPlacedVolume *const placement = NULL) const override;
};

} // End namespace tgeo2vecgeom
} // End namespace vecgeom