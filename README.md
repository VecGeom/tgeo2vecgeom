Providing functionality for geometry conversion from TGeo to VecGeom (and vice versa).

Notes:
*   This code was originally part of VecGeom and has been stripped for better isolation and to 
    avoid cyclic dependencies. 
